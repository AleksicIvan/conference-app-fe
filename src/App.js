import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import logo from './img/logo.jpg';
import './App.css';
import "antd/dist/antd.css";
import { Layout, Menu } from 'antd';

import Login from './features/auth/Login';
import Conferences from './features/conferences/Conference';
import Events from './features/events/Events';
import Tracks from './features/tracks/Tracks';
import AgendaDetails from './features/agendaDetails/AgendaDetails';
import EventDetails from './features/events/EventDetails';
import TrackDetails from './features/tracks/TrackDetails';

import { selectAuth, logout } from './features/auth/authSlice';


const { Header, Content } = Layout;

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
function PrivateRoute({ children, ...rest }) {
  const auth = useSelector(selectAuth);
  return (
    <Route
      {...rest}
      render={({ location }) => {
        // auth?.user @ivan todo -> fix when markovski adds proper user mngmnt
        return  auth?.credentials
          ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location }
              }}
            />
          )
        }
      }
    />
  );
}




function App(props) {
  const auth = useSelector(selectAuth);
  const dispatch = useDispatch();

  return (
    <div className="App">
      <Router>
      <Layout className="layout">
        <Header>
          <div className="logo">
            <img src={logo} alt="symorg_logo" />
          </div>
          <Menu
            theme="dark"
            mode="horizontal"
            activeKey={props.activeTab}
            defaultSelectedKeys={[props.activeTab]}
          >
            {auth.credentials
              ? <Menu.Item key="1">
                  <Link
                    to="/login"
                    onClick={_ => {
                      dispatch(logout())
                    }}
                  >
                    Logut
                  </Link>
                </Menu.Item>
              : <Menu.Item key="1">
                  <Link
                    to="/login"
                  >
                    Login
                  </Link>
              </Menu.Item>}
            <Menu.Item key="2">
              <Link
                to="/conference"
              >
                Conference
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link
                to="/tracks"
              >
                Tracks
              </Link>
            </Menu.Item>
            <Menu.Item key="4">
              <Link
                to="/events"
              >
                Events
              </Link>
            </Menu.Item>
            <Menu.Item key="5">
              <Link
                to="/users"
              >
                Users
              </Link>
            </Menu.Item>
          </Menu>
        </Header>
        <Content>
          <div className="site-layout-content">
            <Switch>
              <Route path="/login" exact>
                <Login />
              </Route>
              <PrivateRoute path="/conference" exact>
                <Conferences />
              </PrivateRoute>
              <PrivateRoute path="/conference/:conferenceName/agendas/:agendaId">
                <AgendaDetails />
              </PrivateRoute>
              <PrivateRoute path="/tracks" exact>
                <Tracks />
              </PrivateRoute>
              <PrivateRoute path="/tracks/:trackId">
                <TrackDetails />
              </PrivateRoute>
              <PrivateRoute path="/events" exact>
                <Events />
              </PrivateRoute>
              <PrivateRoute path="/events/:eventId">
                <EventDetails />
              </PrivateRoute>
              <PrivateRoute path="/users">
                <Conferences />
              </PrivateRoute>
            </Switch>
            <br />
            <br />
            <br />
            <br />
          </div>
        </Content>
        {/* <Footer style={{ textAlign: 'center' }}>©2020 Created by Conference App Team using Ant Design</Footer> */}
      </Layout>
    </Router>
    </div>
  );
}

export default App;
