import React from 'react';
import { render, screen, cleanup } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from './app/store';
import App from './App';

afterEach(cleanup);

test('renders App', () => {
  const { getByText, asFragment } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );

  // screen.debug();
  // // screen.getByRole('');

  expect(getByText(/Conference/i)).toBeInTheDocument();
  expect(asFragment()).toMatchSnapshot();

});
