import { configureStore } from '@reduxjs/toolkit';
import authReducer from '../features/auth/authSlice';
import conferencesReducer from '../features/conferences/conferencesSlice';
import eventsReducer from '../features/events/eventsSlice';
import eventReducer from '../features/events/eventSlice';
import tracksReducer from '../features/tracks/tracksSlice';
import trackReducer from '../features/tracks/trackSlice';
import agendaReducer from '../features/agendaDetails/agendaDetailsSlice';


export default configureStore({
  reducer: {
    auth: authReducer,
    conferences: conferencesReducer,
    events: eventsReducer,
    tracks: tracksReducer,
    agenda: agendaReducer,
    event: eventReducer,
    track: trackReducer
  },
})
