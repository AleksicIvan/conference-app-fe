import React from 'react'
import { useHistory } from "react-router-dom";
import moment from 'moment';

import { Divider, Row, Col, Typography, Button} from 'antd';
import { EditOutlined } from '@ant-design/icons';

const { Text, Title, Link } = Typography;


export default props => {
  const history = useHistory();
  const e = props.event;

  return <div key={e.eventId}>
    <Row>
      <Col span={10} offset={7}>
        <Title level={2}>{e?.name}</Title>
        {props.shouldGoToEventDetails
          ? <Button
              type="primary"
              style={{background: "green", borderColor: "green" }}
              onClick={_ => history.push(`/events/${e.eventId}`)}
            >
              Edit
            </Button>
          : null
        }
      </Col>
    </Row>
    <Row>
    <Col span={10} offset={7}>
    <Title level={4}><Text type="secondary">{e?.location}</Text></Title>
    </Col>
  </Row>
  <Row>
    <Col span={10} offset={7}>
    <Title level={4}>
      <Text>{moment(e.startTime, 'DD-MM-YYYY hh:mm').format('DD.MM.YYYY')}</Text>&nbsp;
      <span style={{color: "#40a9ff"}}>({moment(e.startTime, 'DD-MM-YYYY hh:mm').format('hh:mm')}</span> - <span style={{color: "#40a9ff"}}>{moment(e.endTime, 'DD-MM-YYYY hh:mm').format('hh:mm')})</span></Title>
    </Col>
  </Row>
  <br />
  <Row>
    <Col span={10} offset={7}>
    <Title level={4}>Event Type: </Title>
    <Text>{e?.type?.description}</Text>
    </Col>
  </Row>
  <br />
  <Row>
    <Col span={10} offset={7}>
      <Title level={4}>Event Description: </Title>
      <Text>{e?.description}</Text>
    </Col>
  </Row>
  <br />
  <Row>
    <Col span={10} offset={7}>
      <Title level={4}>Speakers: </Title>
      {e?.speakers && e.speakers.map((speaker, i) => {
        return <div>
          <p><Text strong>{speaker?.user?.firstName} {speaker?.user?.lastName}, {speaker?.title}</Text></p>
          <p><Text>{speaker?.universityOrCompany}, {speaker?.country}</Text></p>
          <p><Link href={`mailto:${speaker?.user?.email}`}>{speaker?.user?.email}</Link></p>
        </div>
      })
      }
    </Col>
  </Row>
  <br />
  <Row>
    <Col span={10} offset={7}>
      <Text keyboard type="warning">{e?.status?.description}</Text>
    </Col>
  </Row>
  <Divider />
</div>
}