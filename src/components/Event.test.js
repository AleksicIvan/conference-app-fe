import React from 'react';
import { render, cleanup } from '@testing-library/react';
import { Provider } from 'react-redux';
import {
    BrowserRouter as Router,
    Route,
  } from "react-router-dom";
import store from '../app/store';
import Event from './Event';

afterEach(cleanup);

test('renders Event related text in Event component', () => {
  const { getByText, asFragment} = render(
    <Provider store={store}>
      <Router>
        <Route>
          <Event event={{eventId: 1}} />
        </Route>
      </Router>
    </Provider>
  );


  expect(getByText(/Event Type:/i)).toBeInTheDocument();
  expect(asFragment()).toMatchSnapshot();
});

