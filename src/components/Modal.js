import React from 'react';
import { Modal, Button } from 'antd';

const ModalComponent = props => {
  return <div>
  <Modal
    title={props.title}
    visible={props.isOpen}
    onOk={props.handleOk}
    onCancel={props.handleCancel}
    okButtonProps={{ disabled: props.okButtonProps.disabled }}
    cancelButtonProps={{ disabled: props.cancelButtonProps.disabled }}
  >
    {props.content}
  </Modal>
  </div>
}

export default ModalComponent;
