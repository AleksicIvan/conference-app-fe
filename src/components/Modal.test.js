import React from 'react';
import { render, cleanup, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import {
    BrowserRouter as Router,
    Route,
  } from "react-router-dom";
import store from '../app/store';
import Modal from './Modal';

afterEach(cleanup);

test('renders Modal', () => {
  const { getByText, asFragment} = render(
    <Provider store={store}>
      <Router>
        <Route>
          <Modal
            isOpen={true}
            content={<h1>Add Agenda</h1>}
            okButtonProps={{disabled: false}}
            cancelButtonProps={{disabled: false}}
          />
        </Route>
      </Router>
    </Provider>
  );

  // screen.debug();


  expect(getByText(/Add Agenda/i)).toBeInTheDocument();
  expect(asFragment()).toMatchSnapshot();
});

