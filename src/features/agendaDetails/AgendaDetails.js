import React, { useEffect, useState } from 'react'
import { Link as Rlink, useParams } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';

import { Space, Row, Col, Typography, DatePicker} from 'antd';
import Modal from '../../components/Modal'
import Event from '../../components/Event'
import { selectAuth } from '../auth/authSlice';
import { selectAgenda, fetchAgenda, updateAgenda } from './agendaDetailsSlice';

const { Text, Title, Link } = Typography;

export default (props) => {
  const auth = useSelector(selectAuth);
  const { isFetching, agenda } = useSelector(selectAgenda);
  const [ agendaDate, setAgendaDate ] = useState(null);
  const dispatch = useDispatch();
  const { agendaId } = useParams()

  useEffect(_ => {
    dispatch(fetchAgenda(agendaId, setAgendaDate));
  }, [])

  const handleDateChangeOnAgendaModal = (date) => {
    setAgendaDate(moment(date, 'DD-MM-YYYY'));
    dispatch(updateAgenda(agendaId, {date: moment(date, 'DD-MM-YYYY').format('DD-MM-YYYY')}, setAgendaDate));
  };

  return <>
    {isFetching
      ? <p>Loading agenda...</p>
      : null
    }

    {// auth?.user @ivan todo -> fix when markovski adds proper user mngmnt
    auth?.credentials
      ? null
      : <p><Rlink to="/login">Login</Rlink></p>
    }

    {!isFetching && agenda
      ? <div>
        <Row>
          <Col span={10} offset={7}>
            <Space direction="vertical">
              <Title>Edit agenda details:</Title>
              <DatePicker
                format="DD-MM-YYYY"
                allowClear={false}
                value={agendaDate}
                onChange={date => {
                  handleDateChangeOnAgendaModal(date)
                }}
              />
            </Space>
          </Col>
        </Row>
    </div>
    : null
    }
  </>
}