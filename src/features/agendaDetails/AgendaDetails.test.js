import React from 'react';
import { render, fireEvent, cleanup, wait, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import {
    MemoryRouter,
    Route,
  } from "react-router-dom";
import store from '../../app/store';
import AgendaDetails  from './AgendaDetails';

afterEach(cleanup);

test('renders Loading agenda text in AgendaDetails component', () => {
  const { getByText, asFragment } = render(
    <Provider store={store}>
      <MemoryRouter>
        <Route>
          <AgendaDetails />
        </Route>
      </MemoryRouter>
    </Provider>
  );

  expect(getByText(/Loading agenda.../i)).toBeInTheDocument();
  expect(asFragment()).toMatchSnapshot();
});

