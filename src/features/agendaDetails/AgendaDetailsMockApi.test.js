import React from 'react';
import { render, fireEvent, cleanup, waitForElement, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import {
    MemoryRouter,
    Route,
  } from "react-router-dom";
import store from '../../app/store';
import AgendaDetails  from './AgendaDetails';
import { loadAgendaSuccess } from './agendaDetailsSlice'

afterEach(cleanup);

test('renders DatePicker control label in AgendaDetails component after API success call', () => {
  const { getByText, asFragment } = render(
    <Provider store={store}>
      <MemoryRouter>
        <Route>
          <AgendaDetails />
        </Route>
      </MemoryRouter>
    </Provider>
  );
  store.dispatch(loadAgendaSuccess({
    agendaId: 7,
    date: "13-09-2020",
    tracks: []
  }));
  expect(getByText(/Edit agenda details/i)).toBeInTheDocument();
  expect(asFragment()).toMatchSnapshot();
});