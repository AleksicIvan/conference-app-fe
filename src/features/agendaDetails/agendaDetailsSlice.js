import { createSlice } from '@reduxjs/toolkit';
import {NotificationManager} from 'react-notifications';
import moment from 'moment';

import axios from '../../services/api'
import getToken from '../../services/auth'

export const agendaDetails = createSlice({
  name: 'agenda',
  initialState: {
    isFetching: false,
    isUpdatingAgenda: false,
    agenda: null
  },
  reducers: {
    loadAgendaRequest: state => {
      state.isFetching = true;
      state.agenda = null;
    },
    loadAgendaSuccess: (state, action) => {
      state.isFetching = false;
      state.agenda = action.payload;
    },
    loadAgendaError: (state, action) => {
      state.isFetching = false;
      state.agenda = null;
    },
    updateAgendaRequest: state => {
      state.isUpdatingAgenda = true;
    },
    updateAgendaSuccess: (state, action) => {
      state.isUpdatingAgenda = false;
    },
    updateAgendaError: (state, action) => {
      state.isUpdatingAgenda = false;
    },
  },
});

export const {
  loadAgendaRequest,
  loadAgendaSuccess,
  loadAgendaError,
  updateAgendaRequest,
  updateAgendaSuccess,
  updateAgendaError,
} = agendaDetails.actions;

export const fetchAgenda = (agendaId, agendaDateSetter) => async (dispatch) => {
  dispatch(loadAgendaRequest())
  try {
    const response = await axios(getToken()).get(`/api/v1/agendas/${agendaId}`)
    dispatch(loadAgendaSuccess(response.data))
    agendaDateSetter(moment(response.data.date, 'DD-MM-YYYY'))
  } catch (error) {
    dispatch(loadAgendaError())
  }
}

export const updateAgenda = (agendaId, data, agendaDateSetter) => async (dispatch) => {
  dispatch(updateAgendaRequest());
  try {
    const response = await axios(getToken()).put(`api/v1/agendas/${agendaId}`, data);
    NotificationManager.success('Agenda Updated', 'Success');
    dispatch(updateAgendaSuccess(response));
    await dispatch(fetchAgenda(agendaId, agendaDateSetter));
  } catch (error) {
    dispatch(updateAgendaError(error));
  }
}

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectAgenda = state => state.agenda;

export default agendaDetails.reducer;
