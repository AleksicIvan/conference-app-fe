import React, { useState } from 'react';
import { Formik, Form, useField } from 'formik';
// import * as Yup from 'yup';
import { Input, Button, Row, Col } from 'antd'
import {
  useHistory,
  useLocation
} from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import {
  selectAuth,
  fetchUser
} from '../auth/authSlice';


const MyTextInput = ({ label, ...props }) => {
  // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
  // which we can spread on <input> and also replace ErrorMessage entirely.
  const [field, meta] = useField(props);
  return (
    <>
      <label htmlFor={props.id || props.name}>{label}</label>
      <Input id={props.id || props.name} className="text-input" {...field} {...props} onChange={props.onChange} />
      {meta.touched && meta.error ? (
        <div style={{color: "red"}}>{meta.error}</div>
      ) : null}
    </>
  );
};


export default () => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()
  const auth = useSelector(selectAuth);
  let history = useHistory();
  let location = useLocation();

  let { from } = location.state || { from: { pathname: "/conference" } };

  return (
    <>
      <Formik
        initialValues={{
          username,
          password,
        }}
        // validationSchema={Yup.object({
        //   password: Yup.string()
        //     .required('Required'),
        //   username: Yup.string()
        //     .required('Required'),
        // })}
        onSubmit={(values, { setSubmitting }) => {
          dispatch(fetchUser({username, password}, history, from))
        }}
      >
      {props => <Form>
          <Row>
            <Col span={10} offset={7}>
              <MyTextInput
                label="Username"
                name="username"
                type="text"
                value={username}
                onChange={e => setUsername(e.target.value)}
              />
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={10} offset={7}>
              <MyTextInput
                label="Password"
                name="password"
                type="password"
                value={password}
                onChange={e => setPassword(e.target.value)}
              />
            </Col>
          </Row>
          <br />
          <Button
            type="submit"
            onClick={_ => dispatch(fetchUser({username, password}, history, from))}
            disabled={auth?.isLoadingCredentials}
          >
            Submit
          </Button>
        </Form>}
      </Formik>
    </>
  );
};