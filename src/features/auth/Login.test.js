import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import {
    MemoryRouter,
    Route,
  } from "react-router-dom";
import store from '../../app/store';
import Login from './Login';

afterEach(cleanup);

test('Submit, Username and Password text can be found in Login component', () => {
  const { getByText, getByLabelText, asFragment } = render(
    <Provider store={store}>
      <MemoryRouter>
        <Route>
          <Login />
        </Route>
      </MemoryRouter>
    </Provider>
  )

  const userNameInput = getByLabelText(/username/i);
  const passwordNameInput = getByLabelText(/password/i);
  const submitButtonText = getByText(/Submit/i);

  expect(submitButtonText).toBeInTheDocument();
  expect(getByText(/Username/i)).toBeInTheDocument();
  expect(getByText(/Password/i)).toBeInTheDocument();
  expect(userNameInput).toBeInTheDocument();

  fireEvent.change(userNameInput, { target: { value: 'aleksic' } });
  expect(userNameInput.value).toBe('aleksic');

  fireEvent.change(passwordNameInput, { target: { value: 'test123' } });
  expect(passwordNameInput.value).toBe('test123');

  fireEvent.click(submitButtonText.closest('button'));
  expect(submitButtonText.closest('button')).toHaveAttribute('disabled');

  expect(getByLabelText(/password/i)).toBeInTheDocument();
  expect(asFragment()).toMatchSnapshot();
});

