import React from 'react';
import { Button } from 'antd';

import { useDispatch } from 'react-redux';
import { logout } from './authSlice';

export default () => {
  const dispatch = useDispatch();
  return <Button onClick={_ => dispatch(logout())}>Logout!</Button>
};