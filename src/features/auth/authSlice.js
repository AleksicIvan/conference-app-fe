import { createSlice } from '@reduxjs/toolkit';
import axios from '../../services/api'


const getLocalStorageCredentials = _ => {
  const cred = JSON.parse(localStorage.getItem('credentials'))
  if (!cred) {
    return null
  }

  return cred
}

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isLoadingUser: false,
    isLoadingCredentials: false,
    user: null,
    credentials: getLocalStorageCredentials()
  },
  reducers: {
    loginRequest: (state, action) => {
      state.isLoadingCredentials = true
      state.user = null
      state.credentials = null
    },
    loginSuccess: (state, action) => {
      localStorage.setItem('credentials', JSON.stringify(action.payload))
      state.isLoadingCredentials = false
      state.user = null
      state.credentials = action.payload
    },
    loginError: (state, action) => {
      state.isLoadingCredentials = false
      state.user = null
      state.credentials = null
    },
    getUserRequest: (state, action) => {
      state.isLoadingUser = true
      state.user = null
      state.credentials = null
    },
    getUserSuccess: (state, action) => {
      state.isLoadingUser = false
      state.user = action.payload
    },
    getUserError: (state, action) => {
      state.isLoadingUser = true
      state.user = null
      state.credentials = null
    },
    logout: (state, action) => {
      localStorage.removeItem('credentials');
      state.isLoadingCredentials = false
      state.isLoadingUser = false
      state.user = null
      state.credentials = null
    },
  },
});


export const {
  loginRequest,
  loginSuccess,
  loginError,
  getUserRequest,
  getUserSuccess,
  getUserError,
  logout
} = authSlice.actions;

export const fetchUser = (userData, history, from) => async dispatch => {
  dispatch(loginRequest())
  try {
    const response = await axios().post('/auth/signin', userData)
    dispatch(loginSuccess(response.data))
    history.replace(from)
  } catch (error) {
    dispatch(loginError())
  }
}

export const selectAuth = state => state.auth;

export default authSlice.reducer;
