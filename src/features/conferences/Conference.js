import React, { useEffect, useState } from 'react';
import { Link, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';

import { Divider, Row, Col, Button, DatePicker, Table, Tooltip, Tag, Typography} from 'antd';
import Modal from '../../components/Modal'
import { DownCircleTwoTone, UpCircleTwoTone, EditOutlined } from '@ant-design/icons'
import { selectAuth } from '../auth/authSlice';
import { selectConferences, fetchConferences, addAgenda } from './conferencesSlice';
import { eventStatuses } from '../../helpers/constants';
const { Text, Title } = Typography;

export const AddNewAgendaModal = ({
  isAddAgendaOpen,
  conferenceId,
  handleOkAddAgendaModal,
  setIsAgendaOpen,
  isOkAddAgendaModalButtonDisabled,
  isCancelAddAgendaModalButtonDisabled,
  handleDateChangeOnAgendaModal
}) => {
  return  <Modal
    title="Add Agenda"
    isOpen={isAddAgendaOpen}
    handleOk={() => handleOkAddAgendaModal(conferenceId)}
    handleCancel={() => {
      setIsAgendaOpen(false)
    }}
    okButtonProps={{
      disabled: isOkAddAgendaModalButtonDisabled
    }}
    cancelButtonProps={{
      disabled: isCancelAddAgendaModalButtonDisabled
    }}
    content={<DatePicker onChange={handleDateChangeOnAgendaModal} />}
  />
};

const getColumns = history => ([
  {
    title: 'Start',
    dataIndex: 'startTime',
    key: 'startTime',
    render: text => <Text strong style={{color: "#40a9ff"}}>{moment(text, 'DD-MM-YYYY hh:mm').format('hh:mm')}</Text>,
    width: '5%',
  },
  {
    title: 'End',
    dataIndex: 'endTime',
    key: 'endTime',
    render: text => <Text strong style={{color: "#40a9ff"}}>{moment(text, 'DD-MM-YYYY hh:mm').format('hh:mm')}</Text>,
    width: '5%',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: text => <Text>{text}</Text>,
    width: '30%',
  },
  {
    title: 'Location',
    dataIndex: 'location',
    key: 'location',
    width: '10%',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    render: st => <Tag color={eventStatuses.filter(status => status.value === st?.description)[0].color} key={st?.description}>{st?.description}</Tag>,
    responsive: ['md'],
    width: '10%',
  },
  {
    title: 'Description',
    dataIndex: 'description',
    key: 'description',
    ellipsis: true,
    render: text => <Tooltip placement="topLeft" title={text}>{text}</Tooltip>,
    responsive: ['md'],
    width: '30%',
  },
  {
    title: 'Action',
    dataIndex: 'eventId',
    key: 'edit',
    // ellipsis: true,
    render: eventId => <EditOutlined style={{color: 'green'}} onClick={_ => history.push(`/events/${eventId}`)} />,
    responsive: ['md'],
    width: '10%',
  },
]);


export default (props) => {
  const [initialCollapsed, setInitialCollapsed] = useState(true);
  const [isAddAgendaOpen, setIsAgendaOpen] = useState(false);
  const [newAgendaDate, setNewAgendaDate] = useState('');
  const [isOkAddAgendaModalButtonDisabled, setIsOkAddAgendaModalButtonDisabled] = useState(false);
  const [isCancelAddAgendaModalButtonDisabled, setIsCancelAddAgendaModalButtonDisabled] = useState(false);
  const [collapsed, setCollapsed] = useState([]);
  const history = useHistory();
  const auth = useSelector(selectAuth);
  const { isFetching, conference} = useSelector(selectConferences);
  const dispatch = useDispatch();

  const handleCollapsedSetting = (agendaId) => {
    setInitialCollapsed(false);
    setCollapsed(collapsed.includes(agendaId)
      ? collapsed.filter(e => e !== agendaId)
      : [...collapsed, agendaId]);
  };

  const handleOkAddAgendaModal = confId => {
    dispatch(addAgenda(confId, {
      date: newAgendaDate,
      tracks: []
    }));
    setIsAgendaOpen(false)
  };

  const handleDateChangeOnAgendaModal = (date) => {
    setNewAgendaDate(moment(date).format('DD-MM-YYYY'));
  };

  useEffect(_ => {
    dispatch(fetchConferences())
  }, []);

  return <>
    {isFetching
      ? <p>Loading conference...</p>
      : null
    }

    {// auth?.user @ivan todo -> fix when markovski adds proper user mngmnt
    auth?.credentials
      ? null
      : <p><Link to="/login">Login</Link></p>
    }

    {!isFetching && conference
      ? <div>
        <AddNewAgendaModal
          isAddAgendaOpen={isAddAgendaOpen}
          setIsAgendaOpen={setIsAgendaOpen}
          handleOkAddAgendaModal={handleOkAddAgendaModal}
          conferenceId={conference.conferenceId}
          isOkAddAgendaModalButtonDisabled={isOkAddAgendaModalButtonDisabled}
          isCancelAddAgendaModalButtonDisabled={isCancelAddAgendaModalButtonDisabled}
          handleDateChangeOnAgendaModal={handleDateChangeOnAgendaModal}
        />
        <Row>
          <Col span={20} offset={2}>
            <Title level={1}>{conference?.name} {moment(conference?.startDate, "DD-MM-YYYY").format("DD.MM.YYYY")} - {moment(conference?.endDate, "DD-MM-YYYY").format("DD.MM.YYYY")}</Title>
          </Col>
        </Row>
        <Row>
          <Col span={10} offset={7}>
            <h2>{conference?.location}</h2>
          </Col>
        </Row>
        <Row>
          <Col span={10} offset={7}>
            <Button type="primary" onClick={_ => setIsAgendaOpen(true)}>Add Agenda</Button>
          </Col>
        </Row>
        <br />
        {conference?.agendas.map((agenda, i) => {
          return <div>
            <Row>
              <Col span={10} offset={7}>
                <div onClick={_ => handleCollapsedSetting(agenda.agendaId)}>
                  <Title level={2}
                    style={{color: "#40a9ff"}}
                  >
                    {agenda?.date}&nbsp;
                  {!collapsed.includes(agenda.agendaId) || initialCollapsed
                    ? null
                    : <EditOutlined
                        style={{color: "green" }}
                        onClick={_ => history.push(`/conference/${conference.name}/agendas/${agenda.agendaId}`)}
                      />
                  }
                  </Title>
                  {!collapsed.includes(agenda.agendaId) || initialCollapsed
                      ? <h1><DownCircleTwoTone /></h1>
                      : <h1><UpCircleTwoTone /></h1>
                    }
                </div>
              </Col>
            </Row>
            <Row justify="start">
              <Col span={10} offset={7}>
              </Col>
            </Row>
            <br />
            {!collapsed.includes(agenda.agendaId) || initialCollapsed
              ? null
              : agenda?.tracks && agenda.tracks.map((track, idx) => {
                return <>
                    <h3>
                      <strong>Track: </strong>
                      {track?.trackType?.description}&nbsp;
                      <EditOutlined
                        style={{color: "green" }}
                        onClick={_ => history.push(`/tracks/${track.trackId}`)}
                      />
                    </h3>

                    <br />
                    {track.events.length > 0
                      ? <>
                        <Table
                          dataSource={track.events}
                          columns={getColumns(history)}
                          pagination={track.events.length > 10}
                          size="small"
                        />
                        <br />
                      </>
                      : <Title level={4}>No events, yet</Title>
                    }

                    {/* {track?.events.map(event => <div>
                      <Divider />
                    <p>
                      <strong>
                        <h4>{event?.name}</h4>&nbsp;
                        <span style={{color: "#40a9ff"}}>{moment(event.startTime, 'DD-MM-YYYY hh:mm').format('hh:mm')}</span> - <span style={{color: "#40a9ff"}}>{moment(event.endTime, 'DD-MM-YYYY hh:mm').format('hh:mm')}</span>
                      </strong>
                    </p>
                    <p
                    // style={{textAlign: "left"}}
                    >
                      {event?.description}
                    </p>
                    </div>
                    )} */}
                  </>
              })
            }
          <Divider />
          </div>
        })
        }
      </div>
      : null
    }
  </>
}