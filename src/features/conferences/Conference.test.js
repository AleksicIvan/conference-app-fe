import React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';
import { Provider } from 'react-redux';
import {
    MemoryRouter,
    Switch,
    Route,
    Link,
    Redirect,
  } from "react-router-dom";
import store from '../../app/store';
import Conference, { AddNewAgendaModal }  from './Conference';

afterEach(cleanup);

test('renders Loading conferences text in Conference component', () => {
  const { getByText, asFragment } = render(
    <Provider store={store}>
      <MemoryRouter>
        <Route>
          <Conference />
        </Route>
      </MemoryRouter>
    </Provider>
  );


  expect(getByText(/Loading conference.../i)).toBeInTheDocument();
  expect(asFragment()).toMatchSnapshot();
});

test('renders AddNewAgendaModal component and change date it after firing onChange events in Conference component', () => {
  const { getByPlaceholderText, asFragment } = render(<AddNewAgendaModal
    isAddAgendaOpen={true}
    handleOkAddAgendaModal={() => {}}
    conferenceId={1}
    setIsAgendaOpen={() => {}}
    isOkAddAgendaModalButtonDisabled={false}
    isCancelAddAgendaModalButtonDisabled={false}
    handleDateChangeOnAgendaModal={() => {}}
  />);


  expect(getByPlaceholderText(/Select date/i)).toBeInTheDocument();
  fireEvent.change(getByPlaceholderText(/Select date/i), { target: { value: '23-06-2020' } });
  expect(getByPlaceholderText(/Select date/i).value).toBe('23-06-2020');
  expect(asFragment()).toMatchSnapshot();
});

