import { createSlice } from '@reduxjs/toolkit';
import {NotificationManager} from 'react-notifications';

import axios from '../../services/api'
import getToken from '../../services/auth'

export const conferencesSlice = createSlice({
  name: 'conferences',
  initialState: {
    isFetching: false,
    isAddingAgenda: false,
    conference: null
  },
  reducers: {
    loadConferenceRequest: state => {
      state.isFetching = true;
      state.conference = null;
    },
    loadConferenceSuccess: (state, action) => {
      state.isFetching = false;
      state.conference = action.payload;
    },
    loadConferenceError: (state, action) => {
      state.isFetching = false;
      state.conference = null;
    },
    addAgendaRequest: state => {
      state.isAddingAgenda = true;
    },
    addAgendaSuccess: (state, action) => {
      state.isAddingAgenda = false;
    },
    addAgendaError: (state, action) => {
      state.isAddingAgenda = false;
    },
  },
});

export const {
  loadConferenceRequest,
  loadConferenceSuccess,
  loadConferenceError,
  addAgendaRequest,
  addAgendaSuccess,
  addAgendaError
} = conferencesSlice.actions;

export const fetchConferences = () => async (dispatch) => {
  dispatch(loadConferenceRequest())
  try {
    const response = await axios(getToken()).get('/api/v1/find/conferences')
    dispatch(loadConferenceSuccess(response.data[0]))
  } catch (error) {
    dispatch(loadConferenceError())
  }
}

export const addAgenda = (confId, data) => async (dispatch) => {
  dispatch(addAgendaRequest());
  try {
    const response = await axios(getToken()).post(`api/v1/${confId}/agendas`, data);
    NotificationManager.success('New Agenda Added', 'Success');
    dispatch(addAgendaSuccess(response));
    await dispatch(fetchConferences());
  } catch (error) {
    dispatch(addAgendaError());
  }
}

export const selectConferences = state => state.conferences;

export default conferencesSlice.reducer;
