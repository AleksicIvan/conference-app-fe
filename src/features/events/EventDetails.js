import react from 'react'

import React, { useEffect, useState } from 'react'
import { Link as Rlink, useParams } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';

import { Select, Space, DatePicker, Row, Col, Typography, Input, Button} from 'antd';
import { selectAuth } from '../auth/authSlice';
import { selectEvent, fetchEvent, updateEvent, updateEventError } from './eventSlice';
import { eventStatuses, eventTypes } from '../../helpers/constants'

const { Text, Title, Link } = Typography;
const Option = Select.Option;
const { RangePicker } = DatePicker;

export default (props) => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [location, setLocation] = useState('');
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');
  const [type, setType] = useState(null);
  const [status, setStatus] = useState(null);
  const [confId, setConfId] = useState(null);
  const [agendaId, setAgendaId] = useState(null);
  const [trackId, setTrackId] = useState(null);
  const auth = useSelector(selectAuth);
  const { isFetching, event, conferences } = useSelector(selectEvent);
  const dispatch = useDispatch();
  const { eventId } = useParams()

  // const getConferenceAgendas = _ => {
  //   if (confId && conferences.length > 0) {
  //     const conference = conferences.filter(c => c.conferenceId === confId)[0]
  //     return conference.agendas
  //   }

  //   return []
  // }


  // const getAgendaTracks = _ => {
  //   if (confId && agendaId) {
  //     const conference = conferences.filter(c => c.conferenceId === confId)[0]
  //     const agenda = conference.agendas && conference.agendas.filter(a => a.agendaId === agendaId)[0]
  //     return agenda.tracks
  //   }

  //   return []
  // }

  const handleUpdateEvent = _ => {
    dispatch(updateEvent(event?.eventId, {
      name,
      description,
      location,
      startTime: moment(startTime).format("DD-MM-YYYY hh:mm"),
      endTime: moment(endTime).format("DD-MM-YYYY hh:mm"),
      type: {
        description: type
      },
      status: {
        description: status
      }
    }));
  }

  const handleEventDateAndTimePicker = (value, dateString) => {
    setStartTime(moment(value[0]).format("DD-MM-YYYY HH:mm"))
    setEndTime(moment(value[1]).format("DD-MM-YYYY HH:mm"))
  }

  const handleEventDateAndTimePickerOk = (value) => {
    setStartTime(moment(value[0], "DD-MM-YYYY HH:mm"))
    setEndTime(moment(value[1], "DD-MM-YYYY HH:mm"))
  }

  useEffect(_ => {
    dispatch(fetchEvent(eventId, {
      setName,
      setDescription,
      setLocation,
      setStartTime,
      setEndTime,
      setType,
      setStatus
    }))
  }, [])

  return <>
    {isFetching
      ? <p>Loading event...</p>
      : null
    }

    {// auth?.user @ivan todo -> fix when markovski adds proper user mngmnt
    auth?.credentials
      ? null
      : <p><Rlink to="/login">Login</Rlink></p>
    }

    {!isFetching && event
      ? <div key={event.eventId}>
      <Space direction="vertical">
        <Row>
          <Col span={15} offset={4}>
            <Title>Edit event details:</Title>
          </Col>
        </Row>
          {/* <Row>
            <Col span={10} offset={7}>
              <Select defaultValue="Conference" style={{ width: 120 }} onChange={v => setConfId(v)}>
                {conferences.map(conf => <Option value={conf.conferenceId}>{conf.name}</Option>)}
              </Select>
            </Col>
          </Row>
          <Row>
            <Col span={10} offset={7}>
              <Select defaultValue="Agenda" style={{ width: 120 }} onChange={v => setAgendaId(v)}>
                {getConferenceAgendas()
                  .map(agenda => <Option value={agenda.agendaId}>{agenda.date}</Option>)
                }
              </Select>
            </Col>
          </Row>
          <Row>
            <Col span={10} offset={7}>
              <Select defaultValue="Track" style={{ width: 120 }} onChange={v => setTrackId(v)}>
                {getAgendaTracks()
                  .map(track => <Option value={track.trackId}>{track?.trackType?.description}</Option>)
                }
              </Select>
            </Col>
          </Row> */}
          <Row>
            <Col span={15} offset={4}>
              <Text>Date & Time</Text>
              <div>
                <RangePicker
                  value={[startTime, endTime]}
                  showTime={{ format: 'HH:mm' }}
                  format="DD-MM-YYYY HH:mm"
                  onChange={handleEventDateAndTimePicker}
                  onOk={handleEventDateAndTimePickerOk}
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col span={15} offset={4}>
              <Select defaultValue={status} style={{ width: 120 }} onChange={v => setStatus(v)}>
                {eventStatuses
                  .map(st => <Option value={st.value}>{st.value}</Option>)
                }
              </Select>
            </Col>
          </Row>
          <Row>
            <Col span={15} offset={4}>
              <Select defaultValue={type} style={{ width: 120 }} onChange={v => setType(v)}>
                {eventTypes
                  .map(e => <Option value={e.value}>{e.value}</Option>)
                }
              </Select>
            </Col>
          </Row>
          <Row>
            <Col span={15} offset={4}>
              <Text>Name</Text>
              <Input.TextArea value={name} onChange={e => {
                setName(e.target.value)
              }} />
            </Col>
          </Row>
          <Row>
            <Col span={15} offset={4}>
              <Text>Description</Text>
              <Input.TextArea value={description} onChange={e => setDescription(e.target.value)} />
            </Col>
          </Row>
          <Row>
            <Col span={15} offset={4}>
              <Text>Location</Text>
              <Input value={location} onChange={e => setLocation(e.target.value)} />
            </Col>
          </Row>
          <Row>
            <Col span={15} offset={4}>
              <Button onClick={handleUpdateEvent}>Ok</Button>
            </Col>
          </Row>
        </Space>
    </div>
    : null
    }
  </>
}