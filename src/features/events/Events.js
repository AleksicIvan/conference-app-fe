import React, { useEffect, useState } from 'react';
import { Link as Rlink, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';

import { Select, Row, Col, Typography, Input, DatePicker, Button, Table, Tooltip, Tag} from 'antd';
import { EditOutlined } from '@ant-design/icons'
import Modal from '../../components/Modal'
import { selectAuth } from '../auth/authSlice';
import { selectEvents, fetchEvents, addEvent } from './eventsSlice';
import { eventStatuses, eventTypes } from '../../helpers/constants'

const { Text, Title, Link } = Typography;
const Option = Select.Option;
const { RangePicker } = DatePicker;


export const AddEventModal = ({
  isAddEventModalOpen,
  handleOkAddEventModal,
  setIsAddEventModalOpen,
  isOkAddEventModalButtonDisabled,
  isCancelAddEventModalButtonDisabled,
  conferences,
  history
}) => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [location, setLocation] = useState('');
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');
  const [type, setType] = useState(null);
  const [status, setStatus] = useState(null);
  const [confId, setConfId] = useState(null);
  const [agendaId, setAgendaId] = useState(null);
  const [trackId, setTrackId] = useState(null);

  const getConferenceAgendas = _ => {
    if (confId && conferences.length > 0) {
      const conference = conferences.filter(c => c.conferenceId === confId)[0]
      return conference.agendas
    }

    return []
  }


  const getAgendaTracks = _ => {
    if (confId && agendaId) {
      const conference = conferences.filter(c => c.conferenceId === confId)[0]
      const agenda = conference.agendas && conference.agendas.filter(a => a.agendaId === agendaId)[0]
      return agenda.tracks
    }

    return []
  }

  const handleEventDateAndTimePicker = (value, dateString) => {
    console.log('Selected Time: ', value);
    console.log('Formatted Selected Time: ', dateString);
  }

  const handleEventDateAndTimePickerOk = (value) => {
    console.log('All date and time: ', value);
    setStartTime(moment(value[0]).format("DD-MM-YYYY HH:mm"))
    setEndTime(moment(value[1]).format("DD-MM-YYYY HH:mm"))
  }

  return <Modal
    title="Add Event"
    isOpen={isAddEventModalOpen}
    handleOk={() => handleOkAddEventModal(
      confId,
      agendaId,
      trackId,
      history,
      {
        status: {
          description: status
        },
        name,
        description,
        location,
        startTime,
        endTime,
        type: {
          description: type
        },
        speakers: null,
        attendees: null
      })}
    handleCancel={() => {
      setName('')
      setDescription('')
      setLocation('')
      setStartTime('')
      setEndTime('')
      setStatus('')
      setType('')
      setConfId(null)
      setAgendaId(null)
      setTrackId(null)
      setIsAddEventModalOpen(false)
    }}
    okButtonProps={{
      disabled: isOkAddEventModalButtonDisabled
    }}
    cancelButtonProps={{
      disabled: isCancelAddEventModalButtonDisabled
    }}
    content={<>
      {conferences
        ? <>
        <div>
          <Select defaultValue="Conference" style={{ width: 120 }} onChange={v => setConfId(v)}>
            {conferences.map(conf => <Option value={conf.conferenceId}>{conf.name}</Option>)}
          </Select>
        </div>
        <br />
        <div>
          <Select defaultValue="Agenda" style={{ width: 120 }} onChange={v => setAgendaId(v)}>
            {getConferenceAgendas()
              .map(agenda => <Option value={agenda.agendaId}>{agenda.date}</Option>)
            }
          </Select>
        </div>
        <br />
        <div>
          <Select defaultValue="Track" style={{ width: 120 }} onChange={v => setTrackId(v)}>
            {getAgendaTracks()
              .map(track => <Option value={track.trackId}>{track?.trackType?.description}</Option>)
            }
          </Select>
        </div>
        <br />
        <div>
        <Select defaultValue="Status" style={{ width: 120 }} onChange={v => setStatus(v)}>
          {eventStatuses
            .map(st => <Option value={st.value}>{st.value}</Option>)
          }
        </Select>
        </div>
        <br />
        <div>
        <Select defaultValue="Type" style={{ width: 120 }} onChange={v => setType(v)}>
          {eventTypes
            .map(e => <Option value={e.value}>{e.value}</Option>)
          }
        </Select>
        </div>
        <br />
        <div>
          <Text>Date & Time</Text>
          <div>
            <RangePicker
              showTime={{ format: 'HH:mm' }}
              format="DD-MM-YYYY HH:mm"
              onChange={handleEventDateAndTimePicker}
              onOk={handleEventDateAndTimePickerOk}
            />
          </div>
        </div>
        <br />
        <div>
          <Text>Name</Text>
          <Input value={name} onChange={e => setName(e.target.value)} />
        </div>
        <br />
        <div>
          <Text>Description</Text>
          <Input.TextArea value={description} onChange={e => setDescription(e.target.value)} />
        </div>
        <br />
        <div>
          <Text>Location</Text>
          <Input value={location} onChange={e => setLocation(e.target.value)} />
        </div>
        <br />
      </>
        : null
      }
    </>}
  />
}

const getColumns = history => ([
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: text => <Text>{text}</Text>,
    width: '20%',
  },
  {
    title: 'Location',
    dataIndex: 'location',
    key: 'location',
    width: '10%',
  },
  {
    title: 'Start',
    dataIndex: 'startTime',
    key: 'startTime',
    render: text => <Text>{moment(text, 'DD-MM-YYYY hh:mm').format('DD-MM-YYYY hh:mm')}</Text>,
    responsive: ['md'],
    width: '10%',
  },
  {
    title: 'End',
    dataIndex: 'endTime',
    key: 'endTime',
    render: text => <Text>{moment(text, 'DD-MM-YYYY hh:mm').format('DD-MM-YYYY hh:mm')}</Text>,
    responsive: ['md'],
    width: '10%',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    render: st => <Tag color={eventStatuses.filter(status => status.value === st?.description)[0].color} key={st?.description}>{st?.description}</Tag>,
    responsive: ['md'],
    width: '10%',
  },
  {
    title: 'Description',
    dataIndex: 'description',
    key: 'description',
    render: text => <Tooltip placement="topLeft" title={text}>{text}</Tooltip>,
    responsive: ['md'],
    width: '30%',
  },
  {
    title: 'Action',
    dataIndex: 'eventId',
    key: 'edit',
    render: eventId => <EditOutlined style={{color: 'green'}} onClick={_ => history.push(`/events/${eventId}`)} />,
    responsive: ['md'],
    width: '10%',
  },
]);

export default (props) => {
  const history = useHistory();
  const [isAddEventModalOpen, setIsAddEventModalOpen] = useState(false)
  const [isOkAddEventModalButtonDisabled, setIsOkAddEventkModalButtonDisabled] = useState(false)
  const [isCancelAddEventModalButtonDisabled, setIsCancelAddEventModalButtonDisabled] = useState(false)
  const auth = useSelector(selectAuth);
  const { isFetching, events, conferences } = useSelector(selectEvents);
  const dispatch = useDispatch();

  const handleOkAddEventModal = (confId, agendaId, trackId, history, data) => {
    dispatch(addEvent(confId, agendaId, trackId, history, data));
    setIsAddEventModalOpen(false)
  };

  useEffect(_ => {
    dispatch(fetchEvents())
  }, [])

  return <>
    {isFetching
      ? <p>Loading events...</p>
      : null
    }

    {// auth?.user @ivan todo -> fix when markovski adds proper user mngmnt
    auth?.credentials
      ? null
      : <p><Rlink to="/login">Login</Rlink></p>
    }

    <Row>
      <Col span={10} offset={7}>
        <Button type="primary" onClick={_ => setIsAddEventModalOpen(true)}>Add Event</Button>
      </Col>
    </Row>
    <br />

    <AddEventModal
      isAddEventModalOpen={isAddEventModalOpen}
      handleOkAddEventModal={handleOkAddEventModal}
      setIsAddEventModalOpen={setIsAddEventModalOpen}
      isOkAddEventModalButtonDisabled={isOkAddEventModalButtonDisabled}
      isCancelAddEventModalButtonDisabled={isCancelAddEventModalButtonDisabled}
      conferences={conferences}
      history={history}
    />


    {!isFetching && events &&
      <Table
        dataSource={events}
        columns={getColumns(history)}
      />
    }
  </>
}