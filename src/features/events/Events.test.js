import React from 'react';
import { render, cleanup } from '@testing-library/react';
import { Provider } from 'react-redux';
import {
    MemoryRouter,
    Route,
  } from "react-router-dom";
import store from '../../app/store';
import Events from './Events';

afterEach(cleanup);


test('renders Events component', () => {
  const { getByText, asFragment } = render(
    <Provider store={store}>
      <MemoryRouter>
        <Route>
          <Events />
        </Route>
      </MemoryRouter>
    </Provider>
  );


  expect(getByText(/Loading events../i)).not.toBeNull();
  expect(getByText(/Loading events../i)).toBeInTheDocument();
  expect(getByText(/Add Event/i)).toBeInTheDocument();
  expect(asFragment()).toMatchSnapshot();
});

