import { createSlice } from '@reduxjs/toolkit';
import {NotificationManager} from 'react-notifications';
import moment from 'moment';

import axios from '../../services/api'
import getToken from '../../services/auth'

export const eventDetails = createSlice({
  name: 'event',
  initialState: {
    isFetching: false,
    isUpdatingEvent: false,
    event: null,
    conferences: null,
  },
  reducers: {
    loadEventRequest: state => {
      state.isFetching = true;
      state.event = null;
      state.conferences = null;
    },
    loadEventSuccess: (state, action) => {
      state.isFetching = false;
      state.event = action.payload.agenda;
      state.conferences = action.payload.conferences;
    },
    loadEventError: (state, action) => {
      state.isFetching = false;
      state.event = null;
      state.conferences = null;
    },
    updateEventRequest: state => {
      state.isUpdatingEvent = true;
    },
    updateEventSuccess: (state, action) => {
      state.isUpdatingEvent = false;
    },
    updateEventError: (state, action) => {
      state.isUpdatingEvent = false;
    },
  },
});

export const {
  loadEventRequest,
  loadEventSuccess,
  loadEventError,
  updateEventRequest,
  updateEventSuccess,
  updateEventError,
} = eventDetails.actions;

export const fetchEvent = (eventId, {
  setName,
  setDescription,
  setLocation,
  setStartTime,
  setEndTime,
  setType,
  setStatus
}) => async (dispatch) => {
  dispatch(loadEventRequest())
  try {
    const token = getToken();
    const conferencesResponse = await axios(token).get('/api/v1/find/conferences');
    const response = await axios(token).get(`/api/v1/events/${eventId}`);
    setName(response?.data?.name);
    setDescription(response?.data?.description);
    setLocation(response?.data?.location);
    setStartTime(moment(response?.data?.startTime, 'DD-MM-YYYY hh:mm'));
    setEndTime(moment(response?.data?.endTime, 'DD-MM-YYYY hh:mm'));
    setEndTime(moment(response?.data?.endTime, 'DD-MM-YYYY hh:mm'));
    setType(response?.data?.type?.description);
    setStatus(response?.data?.status?.description);
    dispatch(loadEventSuccess({agenda: response.data, conferences: conferencesResponse.data}));
  } catch (error) {
    dispatch(loadEventError(error));
  }
}

export const updateEvent = (eventId, data) => async (dispatch) => {
  dispatch(updateEventRequest());
  try {
    const response = await axios(getToken()).put(`api/v1/events/${eventId}`, data);
    NotificationManager.success('Event Updated', 'Success');
    dispatch(updateEventSuccess(response));
    await dispatch(fetchEvent());
  } catch (error) {
    dispatch(updateEventError(error));
  }
}

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectEvent = state => state.event;

export default eventDetails.reducer;
