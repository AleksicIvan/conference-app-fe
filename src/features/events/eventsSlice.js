import { createSlice } from '@reduxjs/toolkit';
import {NotificationManager} from 'react-notifications';

import axios from '../../services/api'
import getToken from '../../services/auth'

export const eventsSlice = createSlice({
  name: 'events',
  initialState: {
    isFetching: false,
    isAddingEvent: false,
    events: null,
    conferences: null
  },
  reducers: {
    loadEventsRequest: state => {
      state.isFetching = true;
      state.events = null;
      state.conferences = null;
    },
    loadEventsSuccess: (state, action) => {
      state.isFetching = false;
      state.events = action.payload.events;
      state.conferences = action.payload.conferences;

    },
    loadEventsError: (state, action) => {
      state.isFetching = false;
      state.events = null;
      state.conferences = null;
    },
    addEventRequest: state => {
      state.isAddingEvent = true;
    },
    addEventSuccess: (state, action) => {
      state.isAddingEvent = false;
    },
    addEventError: (state, action) => {
      state.isAddingEvent = false;
    },
  },
});

export const {
  loadEventsRequest,
  loadEventsSuccess,
  loadEventsError,
  addEventRequest,
  addEventSuccess,
  addEventError,
} = eventsSlice.actions;

export const fetchEvents = () => async (dispatch) => {
  dispatch(loadEventsRequest())
  try {
    const token = getToken()
    const conferencesResponse = await axios(token).get('/api/v1/find/conferences')
    const response = await axios(token).get('/api/v1/find/events')
    dispatch(loadEventsSuccess({ events: response.data, conferences: conferencesResponse.data }))
  } catch (error) {
    dispatch(loadEventsError())
  }
}

export const addEvent = (confId, agendaId, trackId, history, data) => async (dispatch) => {
  dispatch(addEventRequest())
  try {
    const response = await axios(getToken())
      .post(`api/v1/${confId}/agendas/${agendaId}/tracks/${trackId}/events`, data)
    NotificationManager.success('New Event Added', 'Success');
    dispatch(addEventSuccess(response))
    history.push(`/events/${response.data.eventId}`)
    // await dispatch(fetchEvents())
  } catch (error) {
    dispatch(addEventSuccess(error))
  }
}

export const selectEvents = state => state.events;

export default eventsSlice.reducer;
