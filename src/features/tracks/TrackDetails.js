import react from 'react'

import React, { useEffect, useState } from 'react'
import { Link as Rlink, useParams, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';

import { Select, Space, Row, Col, Typography, Input, Button, Table, Tooltip, Tag, Divider} from 'antd';
import { EditOutlined } from '@ant-design/icons'

import { selectAuth } from '../auth/authSlice';
import { selectTrack, fetchTrack, updateTrack } from './trackSlice';
import { trackDescriptionTypes, eventStatuses } from '../../helpers/constants'

const { Text, Title } = Typography;
const Option = Select.Option;


const getColumns = history => ([
  {
    title: 'Start',
    dataIndex: 'startTime',
    key: 'startTime',
    render: text => <Text>{moment(text, 'DD-MM-YYYY hh:mm').format('DD-MM-YYYY hh:mm')}</Text>,
    width: '10%',
  },
  {
    title: 'End',
    dataIndex: 'endTime',
    key: 'endTime',
    render: text => <Text>{moment(text, 'DD-MM-YYYY hh:mm').format('DD-MM-YYYY hh:mm')}</Text>,
    width: '10%',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: text => <Text>{text}</Text>,
    width: '20%',
  },
  {
    title: 'Location',
    dataIndex: 'location',
    key: 'location',
    width: '10%',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    render: st => <Tag color={eventStatuses.filter(status => status.value === st?.description)[0].color} key={st?.description}>{st?.description}</Tag>,
    width: '10%',
    responsive: ['md'],
  },
  {
    title: 'Description',
    dataIndex: 'description',
    key: 'description',
    ellipsis: true,
    render: text => <Tooltip placement="topLeft" title={text}>{text}</Tooltip>,
    width: '30%',
    responsive: ['md'],
  },
  {
    title: 'Action',
    dataIndex: 'eventId',
    key: 'edit',
    render: eventId => <EditOutlined style={{color: 'green'}} onClick={_ => history.push(`/events/${eventId}`)} />,
    width: '10%',
    responsive: ['md'],
  },
]);

export default (props) => {
  const [description, setDescription] = useState('');
  const auth = useSelector(selectAuth);
  const { isFetching, track } = useSelector(selectTrack);
  const dispatch = useDispatch();
  const { trackId } = useParams();
  const history = useHistory();


  const handleUpdateTrack = _ => {
    dispatch(updateTrack(track?.trackId, {
      trackType: {
        description
      },
    }));
  };

  useEffect(_ => {
    dispatch(fetchTrack(trackId, {
      setDescription,
    }))
  }, []);

  return <>
    {isFetching
      ? <p>Loading track...</p>
      : null
    }

    {// auth?.user @ivan todo -> fix when markovski adds proper user mngmnt
    auth?.credentials
      ? null
      : <p><Rlink to="/login">Login</Rlink></p>
    }

    {!isFetching && track
      ? <div key={track.trackId}>
      <Space direction="vertical">
        <Row>
          <Col span={15} offset={4}>
            <Title>Edit track details:</Title>
          </Col>
        </Row>
        <Row>
          <Col span={15} offset={4}>
            <Select defaultValue={description} style={{ width: 120 }} onChange={v => setDescription(v)}>
              {trackDescriptionTypes
                .map(desc => <Option key={desc.value} value={desc.value}>{desc.value}</Option>)
              }
            </Select>
          </Col>
        </Row>
        <Row>
          <Col span={15} offset={4}>
            <Button style={{backgroundColor: 'green', color: 'white'}} onClick={handleUpdateTrack}>Ok</Button>
          </Col>
        </Row>
      </Space>
      <br />
      <br />
      {track?.events?.length > 0
            ? <Table
                dataSource={track.events}
                columns={getColumns(history)}
                pagination={track.events.length > 10}
                size="small"
              />
            : <>
              <Title level={4}>No events, yet</Title>
              <Divider />
            </>
          }
    </div>
    : null
    }
  </>
}