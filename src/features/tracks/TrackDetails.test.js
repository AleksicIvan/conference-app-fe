import React from 'react';
import { render, fireEvent, cleanup, wait, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import {
    MemoryRouter,
    Route,
  } from "react-router-dom";
import store from '../../app/store';
import TrackDetails  from './TrackDetails';

afterEach(cleanup);

test('renders Loading event text in EventDetails component', () => {
  const { getByText, asFragment } = render(
    <Provider store={store}>
      <MemoryRouter>
        <Route>
          <TrackDetails />
        </Route>
      </MemoryRouter>
    </Provider>
  );

  expect(getByText(/Loading track.../i)).toBeInTheDocument();
  expect(asFragment()).toMatchSnapshot();
});

