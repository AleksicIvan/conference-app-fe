import React, { useEffect, useState } from 'react';
import { Link as Rlink , useHistory} from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import { Divider, Select, Row, Col, Typography, Button, Icon, Table, Tooltip, Tag} from 'antd';
import { EditOutlined } from '@ant-design/icons'
import Modal from '../../components/Modal'

import { selectAuth } from '../auth/authSlice';
import { selectTracks, fetchTracks, addTrack } from './tracksSlice';
import { trackDescriptionTypes, eventStatuses } from '../../helpers/constants'

const { Text, Title, Link } = Typography;
const Option = Select.Option;

export const AddTrackModal = ({
  isAddTrackModalOpen,
  handleOkAddTrackModal,
  setIsTrackModalOpen,
  isOkAddTrackModalButtonDisabled,
  isCancelAddTrackModalButtonDisabled,
  conferences
}) => {
  const [description, setDescription] = useState(null)
  const [confId, setConfId] = useState(null)
  const [agendaId, setAgendaId] = useState(null)

  const getConferenceAgendas = _ => {
    if (confId && conferences.length > 0) {
      const conference = conferences.filter(c => c.conferenceId === confId)[0]
      return conference.agendas
    }

    return []
  }

  return <Modal
    title="Add Track"
    isOpen={isAddTrackModalOpen}
    handleOk={() => handleOkAddTrackModal(
      confId,
      agendaId,
      {
        trackType: {
          description
        },
        events: []
      })}
    handleCancel={() => {
      setIsTrackModalOpen(false)
    }}
    okButtonProps={{
      disabled: isOkAddTrackModalButtonDisabled
    }}
    cancelButtonProps={{
      disabled: isCancelAddTrackModalButtonDisabled
    }}
    content={<>
      {conferences
        ? <>
        <Select defaultValue="Conference" style={{ width: 120 }} onChange={v => setConfId(v)}>
          {conferences.map(conf => <Option key={conf.conferenceId} value={conf.conferenceId}>{conf.name}</Option>)}
        </Select>

        <Select defaultValue="Agenda" style={{ width: 120 }} onChange={v => setAgendaId(v)}>
          {getConferenceAgendas()
            .map(agenda => <Option key={agenda.agendaId} value={agenda.agendaId}>{agenda.date}</Option>)
          }
        </Select>

        <Select defaultValue="Description" style={{ width: 120 }} onChange={v => setDescription(v)}>
          {trackDescriptionTypes
            .map(desc => <Option key={desc.value} value={desc.value}>{desc.value}</Option>)
          }
        </Select>
        <br />
      </>
        : null
      }
    </>}
  />
}

const getColumns = history => ([
  {
    title: 'Start',
    dataIndex: 'startTime',
    key: 'startTime',
    render: text => <Text>{moment(text, 'DD-MM-YYYY hh:mm').format('DD-MM-YYYY hh:mm')}</Text>,
    width: '10%',
  },
  {
    title: 'End',
    dataIndex: 'endTime',
    key: 'endTime',
    render: text => <Text>{moment(text, 'DD-MM-YYYY hh:mm').format('DD-MM-YYYY hh:mm')}</Text>,
    width: '10%',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: text => <Text>{text}</Text>,
    width: '20%',
  },
  {
    title: 'Location',
    dataIndex: 'location',
    key: 'location',
    width: '10%',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    render: st => <Tag color={eventStatuses.filter(status => status.value === st?.description)[0].color} key={st?.description}>{st?.description}</Tag>,
    width: '10%',
    responsive: ['md'],
  },
  {
    title: 'Description',
    dataIndex: 'description',
    key: 'description',
    ellipsis: true,
    render: text => <Tooltip placement="topLeft" title={text}>{text}</Tooltip>,
    width: '30%',
    responsive: ['md'],
  },
  {
    title: 'Action',
    dataIndex: 'eventId',
    key: 'edit',
    render: eventId => <EditOutlined style={{color: 'green'}} onClick={_ => history.push(`/events/${eventId}`)} />,
    width: '10%',
    responsive: ['md'],
  },
]);

export default (props) => {
  const history = useHistory();
  const [initalCollapsed, setInitalCollapsed] = useState(true);
  const [collapsedTrackEvents, setCollapsedTrackEvents] = useState([]);
  const [isAddTrackModalOpen, setIsTrackModalOpen] = useState(false);
  const [isOkAddTrackModalButtonDisabled, setIsOkAddTrackModalButtonDisabled] = useState(false);
  const [isCancelAddTrackModalButtonDisabled, setIsCancelAddTrackModalButtonDisabled] = useState(false);
  const auth = useSelector(selectAuth);
  const { isFetching, tracks, conferences } = useSelector(selectTracks);
  const dispatch = useDispatch();

  const handleOkAddTrackModal = (confId, agendaId, data) => {
    dispatch(addTrack(confId, agendaId, data));
    setIsTrackModalOpen(false)
  };

  const handleCollapsedSetting = (trackId) => {
    setInitalCollapsed(false);
    setCollapsedTrackEvents(collapsedTrackEvents.includes(trackId)
      ? collapsedTrackEvents.filter(e => e !== trackId)
      : [...collapsedTrackEvents, trackId]);
  };

  useEffect(_ => {
    dispatch(fetchTracks())
  }, []);

  return <>
    {isFetching
      ? <p>Loading tracks...</p>
      : null
    }

    {// auth?.user @ivan todo -> fix when markovski adds proper user mngmnt
    auth?.credentials
      ? null
      : <p><Rlink to="/login">Login</Rlink></p>
    }

      <Row>
        <Col span={10} offset={7}>
          <Button type="primary" onClick={_ => setIsTrackModalOpen(true)}>Add Track</Button>
        </Col>
      </Row>

      <AddTrackModal
        isAddTrackModalOpen={isAddTrackModalOpen}
        handleOkAddTrackModal={handleOkAddTrackModal}
        setIsTrackModalOpen={setIsTrackModalOpen}
        isOkAddTrackModalButtonDisabled={isOkAddTrackModalButtonDisabled}
        isCancelAddTrackModalButtonDisabled={isCancelAddTrackModalButtonDisabled}
        conferences={conferences}
      />

    {!isFetching && tracks && tracks.map(track => {
      return <div key={track.trackId}>
      <br />
      <Row>
        <Col span={10} offset={7}>
          <Title level={1}>{track?.trackType?.description}&nbsp;
          <EditOutlined
            style={{color: "green" }}
            onClick={_ => history.push(`/tracks/${track.trackId}`)}
          />
          </Title>
        </Col>
      </Row>
      <Title
        onClick={_ => handleCollapsedSetting(track.trackId)}
        level={4}>{
          collapsedTrackEvents.includes(track.trackId)
            ? 'Hide Events'
            : 'Show Events'
        }
      </Title>
      <Divider />
      {!collapsedTrackEvents.includes(track.trackId) || initalCollapsed
        ? null
         : <>
          {track?.events?.length > 0
            ? <Table
                dataSource={track.events}
                columns={getColumns(history)}
                pagination={track.events.length > 10}
                size="small"
              />
            : <>
              <Title level={4}>No events, yet</Title>
              <Divider />
            </>
          }
        </>
      }
   </div>})
    }
  </>
}
