import React from 'react';
import { render, cleanup, fireEvent, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import {
    MemoryRouter,
    Route,
  } from "react-router-dom";
import store from '../../app/store';
import Tracks, { AddTrackModal } from './Tracks';

afterEach(cleanup);


test('renders Loading tracks text in Tracks component', () => {
  const { getByText, asFragment } = render(
    <Provider store={store}>
      <MemoryRouter>
        <Route>
          <Tracks />
        </Route>
      </MemoryRouter>
    </Provider>
  );


  expect(getByText(/Loading tracks../i)).toBeInTheDocument();
  expect(asFragment()).toMatchSnapshot();
});

test('renders AddTrackModal with appropriate control fields in Tracks component', () => {
  const { getByText, getByTestId, getByRole, asFragment } = render(<AddTrackModal
    isAddTrackModalOpen={true}
    handleOkAddTrackModal={() => {}}
    setIsTrackModalOpen={() => {}}
    conferences={[{conferenceId: 1, name: "Symorg"}]}
    isOkAddTrackModalButtonDisabled={false}
    isCancelAddTrackModalButtonDisabled={false}
  />);

  expect(getByText(/Conference/i)).toBeInTheDocument();
  expect(getByText(/Agenda/i)).toBeInTheDocument();
  expect(getByText(/Description/i)).toBeInTheDocument();
  expect(asFragment()).toMatchSnapshot();
});

