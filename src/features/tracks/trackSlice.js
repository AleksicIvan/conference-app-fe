import { createSlice } from '@reduxjs/toolkit';
import {NotificationManager} from 'react-notifications';
import moment from 'moment';

import axios from '../../services/api'
import getToken from '../../services/auth'

export const trackDetails = createSlice({
  name: 'track',
  initialState: {
    isFetching: false,
    isUpdatingTrack: false,
    track: null,
  },
  reducers: {
    loadTrackRequest: state => {
      state.isFetching = true;
      state.track = null;
    },
    loadTrackSuccess: (state, action) => {
      state.isFetching = false;
      state.track = action.payload;
    },
    loadTrackError: (state, action) => {
      state.isFetching = false;
      state.track = null;
    },
    updateTrackRequest: state => {
      state.isUpdatingTrack = true;
    },
    updateTrackSuccess: (state, action) => {
      state.isUpdatingTrack = false;
    },
    updateTrackError: (state, action) => {
      state.isUpdatingTrack = false;
    },
  },
});

export const {
  loadTrackRequest,
  loadTrackSuccess,
  loadTrackError,
  updateTrackRequest,
  updateTrackSuccess,
  updateTrackError,
} = trackDetails.actions;

export const fetchTrack = (trackId, {
  setDescription,
}) => async (dispatch) => {
  dispatch(loadTrackRequest())
  try {
    const token = getToken();
    const response = await axios(token).get(`/api/v1/tracks/${trackId}`);
    setDescription(response?.data?.trackType?.description);
    dispatch(loadTrackSuccess(response.data));
  } catch (error) {
    dispatch(loadTrackError(error));
  }
}

export const updateTrack = (trackId, data) => async (dispatch) => {
  dispatch(updateTrackRequest());
  try {
    const response = await axios(getToken()).put(`api/v1/tracks/${trackId}`, data);
    NotificationManager.success('Track Updated', 'Success');
    dispatch(updateTrackSuccess(response));
    await dispatch(fetchTrack());
  } catch (error) {
    dispatch(updateTrackError(error));
  }
}

export const selectTrack = state => state.track;

export default trackDetails.reducer;
