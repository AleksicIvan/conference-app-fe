import { createSlice } from '@reduxjs/toolkit';
import {NotificationManager} from 'react-notifications';

import axios from '../../services/api';
import getToken from '../../services/auth';

export const tracksSlice = createSlice({
  name: 'tracks',
  initialState: {
    isFetching: false,
    isAddingTrack: false,
    tracks: null,
    conferences: null
  },
  reducers: {
    loadTracksRequest: state => {
      state.isFetching = true;
      state.tracks = null;
      state.conferences = null;
    },
    loadTracksSuccess: (state, action) => {
      state.isFetching = false;
      state.tracks = action.payload.tracks;
      state.conferences = action.payload.conferences;
    },
    loadTracksError: (state, action) => {
      state.isFetching = false;
      state.tracks = null;
      state.conferences = null;
    },
    addTrackRequest: state => {
      state.isAddingTrack = true;
    },
    addTrackSuccess: (state, action) => {
      state.isAddingTrack = false;
    },
    addTrackError: (state, action) => {
      state.isAddingTrack = false;
    },
  },
});

export const {
  loadTracksRequest,
  loadTracksSuccess,
  loadTracksError,
  addTrackRequest,
  addTrackSuccess,
  addTrackError,
} = tracksSlice.actions;

export const fetchTracks = () => async (dispatch) => {
  dispatch(loadTracksRequest())
  try {
    const conferencesResponse = await axios(getToken()).get('/api/v1/find/conferences')
    const response = await axios(getToken()).get('/api/v1/find/tracks')
    dispatch(loadTracksSuccess({tracks: response.data, conferences: conferencesResponse.data}))
  } catch (error) {
    dispatch(loadTracksError())
  }
}

export const addTrack = (confId, agendaId, data) => async (dispatch, getState) => {
  dispatch(addTrackRequest())
  try {
    const response = await axios(getToken())
      .post(`api/v1/${confId}/agendas/${agendaId}/tracks`, data)
    NotificationManager.success('New Track Added', 'Success');
    dispatch(addTrackSuccess(response))
    await dispatch(fetchTracks())
  } catch (error) {
    dispatch(addTrackError(error))
  }
}

export const selectTracks = state => state.tracks;

export default tracksSlice.reducer;
