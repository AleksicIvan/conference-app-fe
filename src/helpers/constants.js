export const ACTIVE_LOGIN = '1'
export const ACTIVE_CONFERENCE = '2'
export const ACTIVE_TRACKS = '3'
export const ACTIVE_EVENTS = '4'
export const ACTIVE_USERS = '5'

export const trackDescriptionTypes = [
  {value: "Session"},
  {value: "Section"},
  {value: "Round Table"},
  {value: "Artificial Intelligence And Education"},
  {value: "Management"},
  {value: "Business"},
]

export const eventStatuses = [
  {value: "In preparation", color: "orange"},
  {value: "In progress", color: "green"},
  {value: "Active", color: "blue"},
  {value: "Deactivated", color: "red"},
]

export const eventTypes = [
  {value: "Lecture"},
  {value: "Break"},
]