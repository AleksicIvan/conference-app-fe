import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import documentReady from 'doc-ready'

import './index.css';
import App from './App';
import store from './app/store';
import { Provider, useSelector, useDispatch } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer} from 'react-notifications';

import {
  ACTIVE_LOGIN,
  ACTIVE_CONFERENCE,
  ACTIVE_TRACKS,
  ACTIVE_EVENTS,
  ACTIVE_USERS
}from './helpers/constants'

const computeActiveTab = path =>  {
  console.log('path: ', path);

  if (path.includes('login')) return ACTIVE_LOGIN;
  if (path.includes('conference')) return ACTIVE_CONFERENCE;
  if (path.includes('tracks')) return ACTIVE_TRACKS;
  if (path.includes('events')) return ACTIVE_EVENTS;
  if (path.includes('users')) return ACTIVE_USERS;
  return ACTIVE_CONFERENCE
};

const AppWrapper = props => {
  const [activeTab, setActiveTab] = useState(computeActiveTab(window.location.pathname));

  useEffect(() => {
    console.log('pusi ga vise');

    setActiveTab(computeActiveTab(window.location.pathname));
  }, [window.location.pathname]);

  return <App activeTab={activeTab} />;
};

documentReady (() => {
  console.info(`Welcome to Conference App, version 0.0.1`)
  ReactDOM.render(
    <React.StrictMode>
      <Provider store={store}>
        <NotificationContainer/>
        <AppWrapper />
      </Provider>
    </React.StrictMode>,
    document.getElementById('root')
  );

  // If you want your app to work offline and load faster, you can change
  // unregister() to register() below. Note this comes with some pitfalls.
  // Learn more about service workers: https://bit.ly/CRA-PWA
  serviceWorker.unregister();
});
