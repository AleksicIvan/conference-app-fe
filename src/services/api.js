import axios from 'axios'
import {NotificationManager} from 'react-notifications';

export default (token) => {
  const newAxios = axios.create({
    baseURL: `http://localhost:8080`
  })

  newAxios.interceptors.response.use(
    function (data) {
      return data;
    },
    function (error) {
      NotificationManager.error(`Code: ${error.response.data.status}; Message: ${error.response.data.message}`, 'Error');
      return Promise.reject(error);
    }
  );

  if (!token) {
    return newAxios;
  }

  newAxios.interceptors.request.use(
    function (config) {
      config.headers.Authorization = `Bearer ${token}`;
      return config;
    }
  );


  return newAxios;
}