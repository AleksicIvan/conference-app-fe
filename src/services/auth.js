const getToken = _ => {
  const creds = JSON.parse(localStorage.getItem('credentials'))
  const token = creds?.token

  return token
}

export default getToken